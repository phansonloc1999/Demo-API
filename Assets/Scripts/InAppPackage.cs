using UnityEngine;

namespace YGame
{
    [System.Serializable]
    public class InAppPackage
    {
        public int id;

        public int gameId;

        public float ammount;

        public bool special;

        public string tab;

        public long valueVND;

        public int typePay;

        public string productId;

        public string ammountString;

        public string icon;

        public string paymentId;

        public string description;

        public int priority;

        public long goldInGame;

        public long endTime;
    }
}